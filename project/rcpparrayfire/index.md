---
title: RcppArrayFire
abstract: ArrayFire for R via Rcpp
categories:
  - R
  - package
  - ArrayFire
#date: "2016-04-27T00:00:00Z"
format:
  html:
    include-in-header:
      - text: |
          <meta http-equiv="refresh" 
                content="0; url=https://daqana.github.io/rcpparrayfire/" />
---
