---
title: swephR
abstract: High precission Swiss Ephemeris for R
categories:
- R
- package
- CRAN
#date: "2016-04-27T00:00:00Z"
format:
  html:
    include-in-header:
      - text: |
          <meta http-equiv="refresh" 
                content="0; url=https://rstub.github.io/swephR/" />
---
