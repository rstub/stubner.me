---
title: tikzDevice
abstract: Create R graphics as TikZ code for use with LaTeX documents
categories:
- R
- package
- CRAN
#date: "2016-04-27T00:00:00Z"
format:
  html:
    include-in-header:
      - text: |
          <meta http-equiv="refresh" 
                content="0; url=https://daqana.github.io/tikzDevice/" />
---
