---
title: dqrng
abstract: Fast Pseudo Random Number Generators for R
categories:
  - R
  - package
  - CRAN
#date: "2016-04-27"
format:
  html:
    include-in-header:
      - text: |
          <meta http-equiv="refresh" 
                content="0; url=https://daqana.github.io/dqrng/" />
---
