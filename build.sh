#!/bin/sh

quarto render --no-clean --output-dir pages
cd pages
git add -A
git commit -m "site update"
git push origin main
