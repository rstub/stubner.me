---
title: Mastodon author attribution for a Quarto blog
author: 'Ralf Stubner'
date: '2024-10-15'
slug: mastodon-author-attribution-quarto
categories:
  - Mastodon
  - Quarto
editor:
  markdown:
    wrap: sentence
    canonical: true
---

Now that [Mastodon 4.3](https://blog.joinmastodon.org/2024/10/mastodon-4.3/) has been released, it makes sense to think about one of the new features there: Author attribution.
The idea is, that when a link from a website is shared on Mastodon[^1], you get an additional link to the author's Fediverse presence:

[^1]: I am not using the more general and in many cases more appropriate "Fediverse" here, because I haven't checked if any other Fediverse software is using this feature (yet).

![](images/clipboard-2963056594.png)

How does one enable that?
I used [Robb Knight's](https://rknight.me/blog/setting-up-mastodon-author-tags/) post as a starting point.
Basically you have to do two things:

-   Add the domain you want to associate your account with under Public Profile \> Verification \> Author attribution: ![](images/clipboard-295131740.png)

-   Add a meta element `<meta name="fediverse:creator" content="@rstub@digitalcourage.social"/>` to the HTML header.

For a Quarto website, the second step can be done using

``` yaml
format:
  html:
    include-in-header:
    - text: |
        <meta name="fediverse:creator" content="@rstub@digitalcourage.social"/>
```

in `_quarto.yml`.
That's all!
